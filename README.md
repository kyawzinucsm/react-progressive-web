# React Progressive Web App

## Install

Clone this project 

You can then install dependencies using either  NPM.

    npm install

## Development

To run the project locally and watch for any changes to the files use:

    npm run watch

This will build the project and serve it locally at `http://localhost:3000`

## Test

To run unit test 


    npm run test:watch

