import pageFactory from './js/components/Page'

export default [
  {
    path: '/',
    exact: true,
    component: pageFactory('top'),
  },
  {
    path: '/newest',
    exact: true,
    component: pageFactory('new'),
  },
]
